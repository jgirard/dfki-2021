FROM alpine:latest AS build
RUN apk update && apk add --no-cache \
gcc \
git \
gmp \
gmp-dev \
libc-dev \
make \
m4 \
ocaml \
ocaml-compiler-libs \
ocaml-ocamldoc \
opam \
perl
RUN git clone https://git.frama-c.com/pub/isaieh.git \
&& cd isaieh \
&& opam init --disable-sandboxing \
&& opam install -y . \
&& eval $(opam env) \
&& make static

FROM jupyter/base-notebook AS runtime
USER root
WORKDIR .
#Installing dependencies
COPY ./requirements.txt .
RUN apt-get update \
&& apt-get install -y unzip wget z3 cmake g++ virtualenv python3.8-dev git \
&& wget https://aisafety.stanford.edu/marabou/marabou-1.0-x86_64-linux.zip \
&& unzip marabou-1.0-x86_64-linux.zip \
&& virtualenv -p /usr/bin/python3.8 ~/.virtualenvs/dfki \
&& source ~/.virtualenvs/dfki/bin/activate \
&& python3 -m pip install -r requirements.txt

#Get ISAIEH executable, pyrat, marabou and the utils
#to the container file system
COPY tutorial.ipynb .
COPY network.onnx .
COPY network.nnet .
COPY formula_lang.py .
COPY sound_pyrat sound_pyrat
COPY NNreader NNreader
COPY logger logger
COPY config.pyc .
COPY onnx2pytorch.py .
COPY pyrat_api.py .
COPY visualize_outputs.py .
COPY doc/imgs/problem_small.png ./doc/imgs/problem_small.png
COPY doc/imgs/network.png ./doc/imgs/network.png
COPY --from=build isaieh/_build/default/bin/converter_static/converter_static.exe ./isaieh.exe
#Exposing port 8888, which is the default for Jupyter
EXPOSE 8888
#Execute jupyter notebook
CMD ["/bin/bash", "-c","source .virtualenvs/dfki/bin/activate && jupyter notebook tutorial.ipynb --allow-root"]
