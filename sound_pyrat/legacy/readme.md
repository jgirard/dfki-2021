This folder contains legacy code that could still prove useful for testing new implementation 
versus old one and trying to implement new method on simpler domains.
This code is mostly not maintained / tested, it is not recommended for reliable analysis.