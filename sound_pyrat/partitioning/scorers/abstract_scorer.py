import abc

from sound_pyrat.analysis_results import SingleAnalysisResults
from sound_pyrat.domains.box import Box


class AbstractScorer(abc.ABC):
    def __init__(self):
        super().__init__()

    def score(self, input_box: Box, result: SingleAnalysisResults = None):
        """
        Return a list of integers between 0 and the number of intervals.
        The highest score is first in the list.

        Default: [0::n]
        """
        return list(range(len(input_box.lower)))

    def __name__(self):
        return "AbstractScorer"
