from config import PROP_DIR, \
    MODELS_DIR
from sound_pyrat.analyzer import analyze
from sound_pyrat.domains.box import Box
from sound_pyrat.partitioning.scorers.abstract_scorer import AbstractScorer
import random

from sound_pyrat.utils import preprocess


# WORK IN PROGRESS
class GradientSmearScorer(AbstractScorer):
    def __init__(self):
        super().__init__()

    @staticmethod
    def score(input_box: Box, model, outputBounds=None):
        if outputBounds is None:
            outputBounds = analyze(input_box, model, max_noise=-1, additional_domains=["zonotopes"])[0]
        print(model[-1])
        gradLower = model[-1]["weight"]
        gradUpper = model[-1]["weight"]
        lastLayerSize = len(gradLower)
        for layer in range(len(model) - 2, -1, -1):
            if "dense" in model[layer]:
                weights = model[layer]["weight"]
                lb = outputBounds[layer].get_lower()
                ub = outputBounds[layer].get_upper()
                layerSize = len(weights[0])
                gradL = [0] * layerSize
                gradU = [0] * layerSize
                for j in range(lastLayerSize):
                    if ub[j] <= 0:
                        gradLower[j], gradUpper[j] = 0, 0
                    elif lb[j] <= 0:
                        gradUpper[j] = gradUpper[j] if gradUpper[j] > 0 else 0
                        gradLower[j] = gradLower[j] if gradLower[j] < 0 else 0
                    for i in range(layerSize):
                        if weights[j][i] >= 0:
                            gradL += weights[j][i] * grad

        indices = list(range(input_box.size))
        random.shuffle(indices)
        return indices


propPath = PROP_DIR / "acasxu/acas_property_3.txt"
modelPath = MODELS_DIR / "acasxu/ACASXU_run2a_5_1_batch_2000.nnet"
model, bounds, toVerify, toCounter = preprocess(propPath, modelPath)
inputInterval = Box(bounds[0], bounds[1])
GradientSmearScorer.score(input_box=inputInterval, model=model)
