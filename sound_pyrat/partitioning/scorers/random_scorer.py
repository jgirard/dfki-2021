from sound_pyrat.domains.box import Box
from sound_pyrat.partitioning.scorers.abstract_scorer import AbstractScorer
import numpy as np


class RandomScorer(AbstractScorer):
    def __init__(self):
        super().__init__()

    def score(self, input_box: Box, *kwargs):
        indices = np.array((range(len(input_box))))
        # separating the case were the interval is reduced to one point: put them last in the ranking
        neq = input_box.upper != input_box.lower
        eq = input_box.upper == input_box.lower
        shuffled = indices[neq]
        np.random.shuffle(shuffled)
        if sum(eq) != 0:
            res = [x for x in shuffled] + [x for x in indices[neq]]
            return res
        return shuffled

    def __name__(self):
        return "RandomScorer"
