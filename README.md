This repository provide all the necessary material for the IDESSAI summer school
tutorial "Formal verification of deep neural networks: Theory and Practice".

The main file is a jupyter notebook `tutorial.ipynb`, slides for the first
part of the tutorial are under `doc/slides.pdf`.

There are several dependancies to install.
A Docker image is provided, as well as detailed installation instructions;
feel free to choose the method you wish.

### Install the tutorial environment within a Docker image

#### Build Docker image
1. Install Docker
    1. on Windows or macOS, install Docker Desktop
    1. on Linux systems, install the Docker toolbox:
        * on Debian/Ubuntu, `sudo apt-get update && sudo apt-get install docker`
        * on Archlinux, `sudo pacman -Syu docker`
        * Don't forget to add your user to the `docker` group if you want
        to run the container as non-root:
        `sudo groupadd docker && sudo usermod -aG docker $USER`
        (you will need to log off for this change to exist)
1. Change directory to be at the root of the tutorial repository
1. Build the docker image: `docker build -t idessai .`. Be advised,
   building the image may take several minutes depending on your internet
   speed and your machine. It will download several software and perform
   updates within the container.

#### Run the tutorial environment
Type `docker run -tp 8888:8888 idessai` to launch the image.
A few URL will pop in your terminal, click on the one
starting with `127.0.0.1`. It
will open a window in your web browser. Double-click on "tutorial.ipynb" and
you are good to go :)

### Install dependencies manually
* python 3.8 is required (no 3.7 nor 3.9) because of pyrat's binaries
* we recommend to create a dedicated python
  environment for the tutorial, using for instance virtualenv:
  `virtualenv -p /usr/bin/python3.8 ~/.virtualenvs/idessai`.
  Then activate the environment using
  `source ~/.virtualenvs/idessai/bin/activate`.
  All subsequent python commands will only affect the environment, keeping
  your system's python installation safe.
* install python dependencies using:
  `python3 -m pip install -r requirements.txt`
* ISAIEH:
  * install the ocaml opam build system
  [available here](https://opam.ocaml.org/doc/Install.html)
  * system dependencies are `m4` and `gmp` at minimum.
  Install them using `sudo apt install m4 libgmp3-dev` on a Debian-like.
  * install ISAIEH using the following command:
  `git clone https://git.frama-c.com/pub/isaieh.git && cd isaieh && opam init --disable-sandboxing && opam install -y . && eval $(opam env) && make`.
* Z3
  * your linux distribution should have a package (named z3), check
  [here](https://github.com/Z3Prover/z3) for a detailed installation
  instruction
* Marabou
  * either get the static binary with
  `wget https://aisafety.stanford.edu/marabou/marabou-1.0-x86_64-linux.zip`,
  or compile from [source](https://github.com/NeuralNetworkVerification/Marabou)
* PyRAT comes as a precompiled binary in this repository that requires Python 3.8

When everything is installed, typing `jupyter notebook tutorial.ipynb` in
a terminal should open the tutorial notebook in your web browser.

### Authors
Julien Girard-Satabin
Guillaume Charpiat
Zakaria Chihani
Augustin Lemesle
Marc Schoenauer
